class_name HelperSpawner
extends Node3D

var IsPlayerInArea = false

@export var HelperRedScene: PackedScene
@export var HelperYellowScene: PackedScene
@export var HelperBlueScene: PackedScene

@onready var HelperScenes: Dictionary = {
	"Red"= HelperRedScene,
	"Yellow" = HelperYellowScene,
	"Blue" = HelperBlueScene
}

@onready var HelperSpawnSpot: Marker3D = $Marker3d

var SpawnQueue: Array
var SpawnTime: float = 0.2

var DespawnQueueRed: Array
var DespawnQueueYellow: Array
var DespawnQueueBlue: Array

func _ready():
	EventBus.OnHelperCountChanged.connect(OnHelperCountChanged.bind())

func _unhandled_input(event):
	if event.is_action_pressed("action") and IsPlayerInArea:
		var despawnColorCount = {
			"Red": DespawnQueueRed.size(),
			"Yellow": DespawnQueueYellow.size(),
			"Blue": DespawnQueueBlue.size()
		}
		EventBus.ShowHelperGetMenu.emit(despawnColorCount)

func OnBodyEntered(body):
	if body is Player:
		IsPlayerInArea = true
		
func OnBodyExited(body):
	if body is Player:
		IsPlayerInArea = false
		
func OnBodyEnteredDespawn(body):
	if body is Helper:
		match body.ColorType:
			"Red":
				DespawnQueueRed.append(body)
			"Yellow":
				DespawnQueueYellow.append(body)
			"Blue":
				DespawnQueueBlue.append(body)
				
func OnBodyExitedDespawn(body):
	if body is Helper:
		match body.ColorType:
			"Red":
				var helperSpot = DespawnQueueRed.find(body)
				if helperSpot != -1:
					DespawnQueueRed.remove_at(helperSpot)
			"Yellow":
				var helperSpot = DespawnQueueYellow.find(body)
				if helperSpot != -1:
					DespawnQueueYellow.remove_at(helperSpot)
			"Blue":
				var helperSpot = DespawnQueueBlue.find(body)
				if helperSpot != -1:
					DespawnQueueBlue.remove_at(helperSpot)
	
func OnHelperCountChanged(difference: Dictionary) -> void:
	for color in difference:
		# We Are Storing If Difference Is Negative
		if (difference[color] < 0):
			for i in abs(difference[color]):
				match color:
					"Red":
						var helper = DespawnQueueRed.pop_front()
						helper.StateMachine.TransitionTo("HelperStateGoHome")
					"Yellow":
						var helper = DespawnQueueYellow.pop_front()
						helper.StateMachine.TransitionTo("HelperStateGoHome")
					"Blue":
						var helper = DespawnQueueBlue.pop_front()
						helper.StateMachine.TransitionTo("HelperStateGoHome")
			continue
		
		# Not Negative Means Spawning
		for i in difference[color]:
			var helper: Helper = HelperScenes[color].instantiate()
			SpawnQueue.append(helper)
	
	if SpawnQueue.size() > 0:	
		get_tree().create_timer(SpawnTime).timeout.connect(SpawnHelper.bind())

func SpawnHelper():
	var helper = SpawnQueue.pick_random()
	var helperIndex = SpawnQueue.find(helper)
	SpawnQueue.remove_at(helperIndex)
	add_sibling(helper)
	helper.global_transform = HelperSpawnSpot.global_transform
	
	if SpawnQueue.size() > 0:
		get_tree().create_timer(SpawnTime).timeout.connect(SpawnHelper.bind())
