class_name Helper
extends CharacterBody3D

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

# Navigation To Control Following The Player
@onready var NavigationAgent: NavigationAgent3D = get_node("NavigationAgent3D")

# Instance Of The StateMachine
# Something might be amiss with this on this scope
@onready var StateMachine = get_node("StateMachine")

# CollisionShape Instance
@onready var CollisionShape: CollisionShape3D = get_node("CollisionShape3d")

@export var ColorType: String

var FollowPoint: Marker3D

var CollisionNumber: Dictionary = {
	"Red": 3,
	"Blue": 4,
	"Yellow": 5
}

func _ready() -> void:
	max_slides = 3
	# Assign The PlayerInstance To All Helper States
	var player = get_tree().get_first_node_in_group("Player")
	for child in StateMachine.get_children():
		child.PlayerInstance = player as Player
		
	if FollowPoint == null:
		FollowPoint = player.HelperFollowPoints.pop_front()

func EnableSameColorCollision(enable: bool):
	set_collision_mask_value(CollisionNumber[ColorType], enable)
	
func _unhandled_input(event: InputEvent) -> void:
	if StateMachine.CurrentState.name == "HelperStateFollow":
		$PositionSprite.visible = false
		return
	
	if event.is_action_pressed("find"):
		EnablePositionSprite()
	elif event.is_action_released("find"):
		DisablePositionSprite()
		
func _process(delta: float) -> void:
	if $PositionSprite.visible:
		var screenPosition = get_viewport().get_camera_3d().unproject_position(global_transform.origin)
		var screenSize = get_viewport().get_visible_rect().size
		screenPosition = screenPosition.clamp(Vector2.ZERO, screenSize)
		$PositionSprite.position = screenPosition

func EnablePositionSprite():
	$PositionSprite.visible = true
	var screenPosition = get_viewport().get_camera_3d().unproject_position(global_transform.origin)
	var screenSize = get_viewport().get_visible_rect().size
	screenPosition = screenPosition.clamp(Vector2.ZERO, screenSize)
	$PositionSprite.position = screenPosition

func DisablePositionSprite():
	$PositionSprite.visible = false
