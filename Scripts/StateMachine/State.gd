class_name State
extends Node

# State Machine Instance
var StateMachine = null

# Override For Handling Inputs
func HandleInput(_event: InputEvent) -> void:
	pass
	
# Override For Update Logic
func Update(_delta: float) -> void:
	pass
	
# Override For Physics Logic
func PhysicsUpdate(_delta: float) -> void:
	pass
	
# Override For Entering A State
func Enter(_message := {}) -> void:
	pass
	
# Override For Exiting A State
func Exit() -> void:
	pass
