class_name HelperState
extends State

# Instance Of The Helper Node
var HelperInstance: Helper

# Instance Of The Player Node
# I Don't Particularly Like The Helper Having
# This, But I'm Not Smart Enough For Another Way
# Maybe Something With An EventBus
var PlayerInstance: Player

# Called when the node enters the scene tree for the first time.
func _ready():
	# Wait Until The Owner Is Ready
	await owner.ready
	
	# Assign HelperInstance To The Owner
	HelperInstance = owner as Helper
	
	# Ensure HelperInstance Is NOT Null
	assert(HelperInstance != null)
