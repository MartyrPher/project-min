class_name HelperStateThrow
extends HelperState

func Enter(_message := {}) -> void:
	print('Entering State Helper Throw')
	var throwTween: Tween = HelperInstance.create_tween()
	var targetCursor = PlayerInstance.ThrowCursor as Cursor
	var target = targetCursor.GetRayCastPoint()
	var delta = (HelperInstance.global_transform.origin - target) / 2
	var middlePoint = Vector3(HelperInstance.global_transform.origin.x - delta.x, 2, HelperInstance.global_transform.origin.z - delta.z)

	throwTween.tween_property(HelperInstance, "global_transform:origin", middlePoint, 0.2).set_trans(Tween.TRANS_QUAD).set_ease(Tween.EASE_IN)
	throwTween.tween_property(HelperInstance, "global_transform:origin", target, 0.2).set_trans(Tween.TRANS_CIRC).set_ease(Tween.EASE_OUT)

	# Re-enable Their Collision
	HelperInstance.CollisionShape.disabled = false
	
func PhysicsUpdate(_delta: float) -> void:
	if HelperInstance.is_on_floor():
		# Once They Touch The Floor Make Them Stand There
		StateMachine.TransitionTo("HelperStateIdle")
