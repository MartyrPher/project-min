class_name HelperStateFollow
extends HelperState

# Speed At Which The Helper Follows The Player
const SPEED = 4

var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

func Enter(_message := {}) -> void:
	SetTargetLocation()
	EventBus.OnHelperEnteredFollow.emit()
	
func Exit() -> void:
	EventBus.OnHelperExitedFollow.emit()
	
func HandleInput(event: InputEvent) -> void:
	if (event.is_action_pressed('unfollow')):
		# Make Them Unfollow The Player By Making Them Idle
		StateMachine.TransitionTo("HelperStateSort")
	
func PhysicsUpdate(_delta: float) -> void:
	if HelperInstance.NavigationAgent.is_navigation_finished():
		HelperInstance.velocity = HelperInstance.velocity.lerp(Vector3.ZERO, 0.2)
		HelperInstance.move_and_slide()
		return
		
	# Get The Next Location
	var nextLocation = HelperInstance.NavigationAgent.get_next_location()
	# Calculate New Velocity
	var velocity = (nextLocation - HelperInstance.transform.origin).normalized()
	velocity.y = 0
	
	if !HelperInstance.is_on_floor():
		velocity.y -= gravity 

	var lookAtTarget = PlayerInstance.global_transform.origin
	lookAtTarget.y = 0
	HelperInstance.look_at(lookAtTarget)
	
	# Finally, Set The Velocity Using The Navigation Agent
	HelperInstance.velocity = velocity * SPEED
	HelperInstance.move_and_slide()

func SetTargetLocation():
	# Get The Target Position, Which IS The Player's Location
	var targetPosition = HelperInstance.FollowPoint.global_transform.origin
	# Set The Target Location On The Navigation Agent
	HelperInstance.NavigationAgent.set_target_location(targetPosition)
	
	CreateTimer()
	
func CreateTimer():
	if StateMachine.CurrentState.name == name:
		get_tree().create_timer(0.1, true, true).timeout.connect(SetTargetLocation.bind())

func OnVelocityComputed(safe_velocity):
	# Assign The Safe Velocity, Also Uses SPEED
	HelperInstance.velocity = safe_velocity
	
	# Move The Helper
	HelperInstance.move_and_slide()
