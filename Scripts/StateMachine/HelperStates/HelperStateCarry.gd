class_name HelperStateCarry
extends HelperState

var TargetPoint: Marker3D
var Target: HelperCarryObject

func Enter(message := {}) -> void:
	print('Entering State Helper Carry')
	Target = message["Object"]
	TargetPoint = message["TargetPoint"]
	Target.UpdateCarryCount(1)

func Exit() -> void:
	HelperInstance.CollisionShape.disabled = false
	if Target == null:
		return
	Target.UpdateCarryCount(-1)

func PhysicsUpdate(_delta: float) -> void:
	if TargetPoint == null:
		StateMachine.TransitionTo("HelperStateIdle")
		return

	HelperInstance.global_transform = TargetPoint.global_transform
	HelperInstance.look_at(Target.CenterMarker.global_transform.origin)
