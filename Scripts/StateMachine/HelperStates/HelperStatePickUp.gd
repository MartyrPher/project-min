class_name HelperStatePickUp
extends HelperState

var Target: Node3D

var TargetPoint: Marker3D

var SPEED: int = 4

var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

func Enter(message := {}) -> void:
	print('Entering State Helper PickUp')
	Target = message["Object"]
	TargetPoint = Target.AssignPoint()

func PhysicsUpdate(_delta: float):
	if HelperInstance.global_transform.origin.distance_to(TargetPoint.global_transform.origin) < 0.3:
		StateMachine.TransitionTo("HelperStateCarry",{"Object": Target, "TargetPoint": TargetPoint })

	# Calculate New Velocity
	var velocity = (TargetPoint.global_transform.origin - HelperInstance.global_transform.origin).normalized()
	velocity.y = 0

	if !HelperInstance.is_on_floor():
		HelperInstance.velocity.y -= gravity

	# Finally, Set The Velocity Using The Navigation Agent
	HelperInstance.velocity = velocity * SPEED
	HelperInstance.move_and_slide()
