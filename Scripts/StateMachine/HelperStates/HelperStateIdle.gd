class_name HelperStateIdle
extends HelperState

var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

func Enter(_message := {}) -> void:
	print('Entering State Helper Idle')
	HelperInstance.velocity = Vector3.ZERO
	
func PhysicsUpdated(_delta: float):
	if HelperInstance.is_on_floor():
		HelperInstance.velocity.y += gravity
	
	HelperInstance.move_and_slide()
	
# They are literally JUST STANDING THERE.
# THERE IS NOTHING THAT THEY DO WHEN IDLE
# Well... Besides DOING NOTHING.
# ONLY THE WHISTLE CAN MAKE THEM START MOVING
# THIS STATE WILL PROBABLY BE USED TO MAKE 
# STUPID ANIMATIONS WHEN THEY ARE STANDING
# THERE DOING ABSOLUTELY NOTHING!!!! 
