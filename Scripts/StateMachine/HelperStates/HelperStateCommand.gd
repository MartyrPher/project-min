class_name HelperStateCommand
extends HelperState

# Speed At Which The Helper Follows The Player
const SPEED = 4

var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

var TargetPoint: Vector3

var WallClimbVelocity: float = 0.25

func Enter(_message := {}) -> void:
	SetTargetLocation()
	
func PhysicsUpdate(_delta: float) -> void:
	if HelperInstance.global_transform.origin.distance_to(TargetPoint) < 0.2:
		StateMachine.TransitionTo("HelperStateIdle")
		
	if HelperInstance.is_on_wall():
		# CLIMB WALL
		var velocity = (TargetPoint - HelperInstance.transform.origin).normalized()
		var wallNormal = HelperInstance.get_wall_normal()
		velocity.y += WallClimbVelocity
		velocity.z = -wallNormal.z
		velocity.x = -wallNormal.x
		HelperInstance.velocity = velocity * SPEED
		HelperInstance.move_and_slide()
		return
	
	# Calculate New Velocity
	var velocity = (TargetPoint - HelperInstance.transform.origin).normalized()
	velocity.y = 0
	
	if !HelperInstance.is_on_floor():
		velocity.y -= gravity 
		
	var lookAtTarget = TargetPoint
	HelperInstance.look_at(lookAtTarget)
	
	# Finally, Set The Velocity Using The Navigation Agent
	HelperInstance.velocity = velocity * SPEED
	HelperInstance.move_and_slide()
	
func SetTargetLocation():
	var targetCursor = PlayerInstance.ThrowCursor as Cursor
	TargetPoint = targetCursor.GetRayCastPoint()
	
func CreateTimer():
	get_tree().create_timer(0.1, true, true).timeout.connect(SetTargetLocation.bind())

func OnVelocityComputed(safe_velocity):
	# Assign The Safe Velocity, Also Uses SPEED
	HelperInstance.velocity = safe_velocity
	
	# Move The Helper
	HelperInstance.move_and_slide()
