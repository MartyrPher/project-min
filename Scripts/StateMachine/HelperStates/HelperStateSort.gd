class_name HelperStateSort
extends HelperState

# Speed At Which The Helper Follows The Player
const SPEED = 4

var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

var TargetPoint: Vector3

func Enter(_message := {}) -> void:
	HelperInstance.EnableSameColorCollision(true)
	SetTargetLocation()
	
func Exit() -> void:
	HelperInstance.EnableSameColorCollision(false)
	
func PhysicsUpdate(_delta: float) -> void:
	if HelperInstance.global_transform.origin.distance_to(TargetPoint) < 0.5:
		StateMachine.TransitionTo("HelperStateIdle")
	
	# Calculate New Velocity
	var velocity = (TargetPoint - HelperInstance.transform.origin).normalized()
	velocity.y = 0
	
	if !HelperInstance.is_on_floor():
		velocity.y -= gravity 
	
	# Finally, Set The Velocity Using The Navigation Agent
	HelperInstance.velocity = velocity * SPEED
	HelperInstance.move_and_slide()
	
func SetTargetLocation():
	TargetPoint = GetSortPoint()
	
func GetSortPoint() -> Vector3:
	match HelperInstance.ColorType:
		"Blue":
			return PlayerInstance.HelperBlueSortMarker.global_transform.origin
		"Red":
			return PlayerInstance.HelperFollowMarker.global_transform.origin
		"Yellow":
			return PlayerInstance.HelperYellowSortMarker.global_transform.origin
	return Vector3.ZERO
