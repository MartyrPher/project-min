class_name HelperStateRunAway
extends HelperState

# Speed At Which The Helper Follows The Player
const SPEED = 4

var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

func Enter(_message := {}) -> void:
	print("Entering Helper State Run Away")
	var randTime = randf_range(0.2, 1)
	get_tree().create_timer(randTime).timeout.connect(TimerTimeout.bind())
	
func PhysicsUpdate(_delta: float) -> void:
	# Calculate New Velocity
	print(HelperInstance.velocity)
	var velocity = -abs(HelperInstance.velocity)
	velocity.y = 0

	if !HelperInstance.is_on_floor():
		velocity.y -= gravity

	# Finally, Set The Velocity Using The Navigation Agent
	HelperInstance.velocity = velocity
	HelperInstance.move_and_slide()
	
func TimerTimeout():
	StateMachine.TransitionTo("HelperStateIdle")
