class_name HelperStateGoHome
extends HelperState

# Speed At Which The Helper Goes Home
const SPEED = 6

var HomeDestination: Vector3

func Enter(_message := {}) -> void:
	print('Entering State Helper Go Home')
	var spawnMarker = get_tree().get_first_node_in_group("SpawnMarker")
	HomeDestination = spawnMarker.global_transform.origin
	PlayerInstance.HelperFollowPoints.push_back(HelperInstance.FollowPoint)
	
func PhysicsUpdate(_delta: float) -> void:
	# Get The Target Position, Which IS The Player's Location
	var targetPosition = HomeDestination
	# Set The Target Location On The Navigation Agent
	HelperInstance.NavigationAgent.set_target_location(targetPosition)
	
	# Get The Next Location
	var nextLocation = HelperInstance.NavigationAgent.get_next_location()
	# Calculate New Velocity
	var velocity = (nextLocation - HelperInstance.transform.origin) * SPEED
	HelperInstance.velocity = HelperInstance.velocity.lerp(velocity, 0.2)
	HelperInstance.velocity.y = 0
	
	# Finally, Set The Velocity Using The Navigation Agent
	HelperInstance.move_and_slide()
	
func OnTargetReached():
	if StateMachine.CurrentState.name == self.name:
		HelperInstance.queue_free()
