class_name HelperStateHeld
extends HelperState

func Enter(_message := {}) -> void:
	print('Entering State Helper Held')
	# Disable The Collision
	# Not Disabling It Messes With The Player Collision
	#HelperInstance.CollisionShape.disabled = true
	HelperInstance.NavigationAgent.avoidance_enabled = false
	
func PhysicsUpdate(_delta: float) -> void:
	# Move The Helper To The Point In Front Of The Player
	HelperInstance.transform.origin = PlayerInstance.HelperHoldMarker.global_transform.origin

func HandleInput(event: InputEvent) -> void:
	if event.is_action_released("throw"):
		# When Throw Is Released Change State To Throw
		StateMachine.TransitionTo("HelperStateThrow")
