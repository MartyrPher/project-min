class_name StateMachine
extends Node

# Event For Transitioning States
signal Transitioned(stateName)

# Initial State Machine State
@export var InitialState := NodePath()

# Current Machine State Set OnReady
@onready var CurrentState : State = get_node(InitialState)

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	# Wait Until The Owner Is Ready
	await owner.ready
	
	# Set The State Machine Instance For Each Child State
	for child in get_children():
		child.StateMachine = self
		
	# Enter The Initial State
	CurrentState.Enter()
	
func _unhandled_input(event) -> void:
	# Call Current State Update Logic
	CurrentState.HandleInput(event)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta) -> void:
	# Call Current State Update Logic
	CurrentState.Update(delta)
	
func _physics_process(delta) -> void:
	# Call Current State Physics Update Logic
	CurrentState.PhysicsUpdate(delta)
	
func TransitionTo(stateName: String, message: Dictionary = {}) -> void:
	# Exit The Current State
	CurrentState.Exit()
	
	# Get The Next State
	CurrentState = get_node(stateName)
	
	# Enter The New State
	CurrentState.Enter(message)
	
	# Emit That The Machine Transitioned States
	emit_signal("Transitioned", stateName)
