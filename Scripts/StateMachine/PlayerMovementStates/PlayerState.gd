class_name PlayerState
extends State

# Instance Of The Player Node
var PlayerInstance : Player

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	# Wait Until The Owner Is Ready
	await owner.ready
	
	# Assign PlayerInstance To The Owner
	PlayerInstance = owner as Player
	
	# Ensire PlayerInstance Is NOT Null
	assert(PlayerInstance != null)
