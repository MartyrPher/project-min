class_name PlayerRunState
extends PlayerState

const SPEED = 400

var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

func Enter(_message := {}) -> void:
	print('Entering State Player Run')

func PhysicsUpdate(delta: float) -> void:
	var lookAtHeight = PlayerInstance.global_transform.origin.y + PlayerInstance.ThrowCursor.global_transform.origin.y
	var throwCursorOrigin = PlayerInstance.ThrowCursor.global_transform.origin
	var lookAt = Vector3(throwCursorOrigin.x, lookAtHeight, throwCursorOrigin.z)
	PlayerInstance.look_at(lookAt)
	
	var distance = PlayerInstance.global_transform.origin.distance_to(PlayerInstance.ThrowCursor.global_transform.origin)
	var targetPosition = Vector3.ZERO
	
	if (distance < 3):
		targetPosition = PlayerInstance.global_transform.origin
	else:
		# Get The Target Position, Which IS The Cursor's Location
		targetPosition = PlayerInstance.ThrowCursor.global_transform.origin
		
	# Set The Target Location On The Navigation Agent
	PlayerInstance.NavigationAgent.set_target_location(targetPosition)
	
	# Get The Next Location
	var nextLocation = PlayerInstance.NavigationAgent.get_next_location()
	
	# Calculate New Velocity
	var velocity = nextLocation - PlayerInstance.transform.origin
	velocity = velocity.normalized()
	PlayerInstance.velocity = velocity * SPEED
	PlayerInstance.velocity.y = 0
	
	# Finally, Set The Velocity Using The Navigation Agent
	PlayerInstance.move_and_slide()

func OnVelocityComputed(safe_velocity):
	# Assign The Safe Velocity, Also Uses SPEED
	PlayerInstance.velocity = PlayerInstance.velocity.lerp(safe_velocity * SPEED, 0.9)
	# Move The Helper
	PlayerInstance.move_and_slide()
