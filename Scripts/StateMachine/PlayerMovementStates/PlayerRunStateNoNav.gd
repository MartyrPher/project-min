class_name PlayerRunStateNoNav
extends PlayerState

const SPEED = 4

var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

func Enter(_message := {}) -> void:
	print('Entering State Player Run')

func PhysicsUpdate(_delta: float) -> void:
	var lookAtHeight = PlayerInstance.global_transform.origin.y #+ PlayerInstance.ThrowCursor.global_transform.origin.y
	var throwCursorOrigin = PlayerInstance.ThrowCursor.global_transform.origin
	var lookAt = Vector3(throwCursorOrigin.x, lookAtHeight, throwCursorOrigin.z)
	
	if !PlayerInstance.global_transform.origin.distance_to(throwCursorOrigin) < 1:
		PlayerInstance.look_at(lookAt)
	
	var direction = Vector3.ZERO
	var camera = get_tree().get_first_node_in_group("Camera")
	var cameraForward = -camera.transform.basis.z.normalized()

	if Input.is_action_pressed("move_up"):
		direction += cameraForward
	if Input.is_action_pressed("move_down"):
		direction += -cameraForward
	if Input.is_action_pressed("move_right"):
		direction += cameraForward.cross(Vector3.UP)
	if Input.is_action_pressed("move_left"):
		direction += -cameraForward.cross(Vector3.UP)

	PlayerInstance.velocity.x = direction.x * SPEED
	PlayerInstance.velocity.z = direction.z * SPEED
	
	if !PlayerInstance.is_on_floor():
		PlayerInstance.velocity.y = -gravity

	PlayerInstance.move_and_slide()
