class_name PlayerIdleState
extends PlayerState

const SPEED = 4

func Enter(_message := {}) -> void:
	print('Entering State Player Idle')

# Override For Physics Logic
func PhysicsUpdate(_delta: float) -> void:
	var lookAtHeight = PlayerInstance.global_transform.origin.y + PlayerInstance.ThrowCursor.global_transform.origin.y
	var throwCursorOrigin = PlayerInstance.ThrowCursor.global_transform.origin
	var lookAt = Vector3(throwCursorOrigin.x, lookAtHeight, throwCursorOrigin.z)
	PlayerInstance.look_at(lookAt)
	
	var distance = PlayerInstance.global_transform.origin.distance_to(PlayerInstance.ThrowCursor.global_transform.origin)

	if (distance > 3):
		StateMachine.TransitionTo("PlayerRunState")
