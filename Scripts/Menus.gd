class_name Menus
extends Control

@export var PackedSceneFullscreenMessage: PackedScene

# Called when the node enters the scene tree for the first time.
func _ready():
	EventBus.OnSynthStationInteracted.connect(ShowSynthMenu.bind())
	EventBus.OnDemoAreaButtonPressed.connect(ShowTransition.bind())
	EventBus.OnHomeBaseButtonPressed.connect(ShowTransition.bind())
	
	EventBus.ShowHelperGetMenu.connect(ShowHelperGetMenu.bind())
	EventBus.ShowMapSelectorMenu.connect(ShowMapSelectorMenu.bind())
	EventBus.OnShowReturnToHomebaseMenu.connect(ShowReturnHomebase.bind())
	EventBus.OnPausedPressed.connect(ShowPauseMenu.bind())
	
	EventBus.OnStaminaReachedHalf.connect(ShowFullscreenMessage.bind("Darkness\nApproaches", 123, 0))
	EventBus.OnStaminaReachedQuarter.connect(ShowFullscreenMessage.bind("Darkness\nCloses\nIn", 123, 0))
	EventBus.OnStaminaDepleted.connect(ShowFullscreenMessage.bind("Darkness Arrived", 255, 0))

	EventBus.OnGoneToSleep.connect(ShowNewDayMessage.bind())
	EventBus.OnMapLoaded.connect(ShowHUD.bind())
	
	$SynthMenu.connect("visibility_changed", ShowHideMouse.bind())
	$HelperGetMenu.connect("visibility_changed", ShowHideMouse.bind())
	$MapSelectorMenu.connect("visibility_changed", ShowHideMouse.bind())
	$PauseMenu.connect("visibility_changed", ShowHideMouse.bind())
	$ConfirmHomeBaseReturnMenu.visibility_changed.connect(ShowHideMouse.bind())

func ShowSynthMenu():
	$SynthMenu.visible = !$SynthMenu.visible

func ShowTransition():
	$Transition.visible = true
	await get_tree().create_timer(1.0).timeout
	$Transition.visible = false
	
func ShowHelperGetMenu(counts: Dictionary) -> void:
	$HelperGetMenu.UpdateCount(counts)
	$HelperGetMenu.visible = true

func ShowMapSelectorMenu() -> void:
	$MapSelectorMenu.visible = !$MapSelectorMenu.visible
	
func ShowReturnHomebase() -> void:
	$ConfirmHomeBaseReturnMenu.visible = !$ConfirmHomeBaseReturnMenu.visible

func ShowPauseMenu() -> void:
	$PauseMenu.visible = !$PauseMenu.visible

func ShowHUD(enable: bool) -> void:
	if enable:
		$HUD.EnableHUD()
		return

	$HUD.DisableHUD()

func ShowFullscreenMessage(message: String, startAlpha: int, endAlpha: int):
	var fullscreenMessage = PackedSceneFullscreenMessage.instantiate()
	fullscreenMessage.Init()
	fullscreenMessage.ChangeAlpha(startAlpha, endAlpha)
	fullscreenMessage.UpdateLabel(message)
	add_child(fullscreenMessage)
	
func ShowNewDayMessage():
	var fullscreenMessage = PackedSceneFullscreenMessage.instantiate()
	fullscreenMessage.Init()
	fullscreenMessage.ChangeAlpha(255, 0)
	
	var message = GameManager.GetCurrentDayString()
	fullscreenMessage.UpdateLabel(message)
	add_child(fullscreenMessage)

func ShowHideMouse() -> void:
	var mouseMode = Input.mouse_mode
	var menuIsActive = false
	
	if mouseMode == Input.MOUSE_MODE_VISIBLE:
		mouseMode = Input.MOUSE_MODE_CONFINED_HIDDEN
		menuIsActive = false
	else:
		mouseMode = Input.MOUSE_MODE_VISIBLE
		menuIsActive = true
	
	GameManager.ChangeMenuIsActive(menuIsActive)
	GameManager.ChangeMouseMode(mouseMode)
