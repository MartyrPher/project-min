class_name GameCamera
extends Node3D

@export var Target: Node3D

var invert_y = false
var MouseSensitivity = 0.01

var CameraMoveThreshold: Vector2
var VisibleRectSize: Vector2

var MinZoom: Vector3 = Vector3(0.6, 0.6, 0.6)
var RegZoom: Vector3 = Vector3(0.9, 0.9, 0.9)
var MaxZoom: Vector3 = Vector3(1.2, 1.2, 1.2)

var CameraZoomJohnTween: Tween

# Called when the node enters the scene tree for the first time.
func _ready():
	# Make The Mouse Confined And Hidden
	GameManager.ChangeMouseMode(Input.MOUSE_MODE_CONFINED_HIDDEN)
	VisibleRectSize = get_viewport().get_visible_rect().size
	CameraMoveThreshold.x = VisibleRectSize.x / 3.5
	CameraMoveThreshold.y = VisibleRectSize.x - (VisibleRectSize.x / 3.5)
	
	$InnerGimbal/Camera.scale = RegZoom

func GetMousePosition3DCoords(mousePosition: Vector2) -> Vector3:
	var spaceState = get_world_3d().direct_space_state
	var rayOrigin =  $InnerGimbal/Camera.project_ray_origin(mousePosition)
	var rayEnd = rayOrigin + $InnerGimbal/Camera.project_ray_normal(mousePosition) * 2000
	var rayParameters = PhysicsRayQueryParameters3D.create(rayOrigin, rayEnd)
	var rayArray = spaceState.intersect_ray(rayParameters)
	if rayArray.has("position"):
		return rayArray["position"]
	return Vector3()

func Get2DCoordsFrom3DPoint(point: Vector3):
	var viewportPoint =  $InnerGimbal/Camera.unproject_position(point)
	return viewportPoint

#Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(_delta):	
	if Target:
		global_transform.origin = Target.global_transform.origin

	var mousePosition = get_viewport().get_mouse_position()
	var direction = 0
	if mousePosition.x < CameraMoveThreshold.x:
		direction = 1
	elif CameraMoveThreshold.y < mousePosition.x:
		direction = -1

	if direction != 0:
		rotate_object_local(Vector3.UP, direction * _delta)

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_released("zoom_in") and event is InputEventMouseButton:
		ZoomCamera(true)
	if event.is_action_released("zoom_out") and event is InputEventMouseButton:
		ZoomCamera(false)

func ZoomCamera(zoom: bool):
	if CameraZoomJohnTween:
		set_process_unhandled_input(false)
		return
	
	var zoomLevel: Vector3
	var currentZoom: float = scale.x
	
	if (currentZoom == MaxZoom.x and !zoom) or (currentZoom == MinZoom.x and zoom):
		return
	
	if zoom and currentZoom == RegZoom.x:
		zoomLevel = MinZoom
	elif !zoom and currentZoom == RegZoom.x:
		zoomLevel = MaxZoom
	else:
		zoomLevel = RegZoom
		
	currentZoom = zoomLevel.x
	
	CameraZoomJohnTween = create_tween().bind_node(self)
	CameraZoomJohnTween.finished.connect(OnCameraTweenFinished.bind())
	
	CameraZoomJohnTween.tween_property(self, "scale", zoomLevel, 0.25)
	
func OnCameraTweenFinished():
	CameraZoomJohnTween = null
	set_process_unhandled_input(true)
