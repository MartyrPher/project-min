class_name Whistle
extends Node3D

# Whistle Collision Shape
var CollisionShape: CylinderShape3D

# Whistle Mesh
var WhistleMesh: Decal

# Whistle Area
var WhistleArea: Area3D

# Maximum Whistle Radius
var MaxWhistleRadius: float = 3

# Minimum Whistle Radius
var MinWhistleRadius: float = 0.01

# Whistle Speed (How Fast Whistle Becomes Max)
var WhistleSpeed: float = 0.04

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	# Get The Nodes
	CollisionShape = $WhistleArea/WhistleShape.get('shape')
	WhistleMesh = $Decal
	WhistleArea = $WhistleArea
	
	# Set Initial Values
	SetWhistleMonitoringAndVisiblilityTo(false)
	SetWhistleCollisionRadiusTo(MinWhistleRadius)
	SetMeshScaleTo(MinWhistleRadius)

func _unhandled_input(event) -> void:
	# Don't Want to Activate Whistle IF a Menu is UP!!!!
	if GameManager.MenuIsActive:
		return
	
	if event.is_action_pressed("whistle"):
		SetWhistleMonitoringAndVisiblilityTo(true)
	elif event.is_action_released("whistle"):
		SetWhistleMonitoringAndVisiblilityTo(false)
		SetWhistleCollisionRadiusTo(MinWhistleRadius)
		SetMeshScaleTo(MinWhistleRadius)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta) -> void:
	# Get Current Values
	var whistleRadius = CollisionShape.get('radius')
	var whistleMeshScale = WhistleMesh.scale
	
	if get_parent().Disabled:
		WhistleMesh.modulate = Color.RED
		$WhistleArea/WhistleShape.disabled = true
	else:
		WhistleMesh.modulate = Color.WHITE
		$WhistleArea/WhistleShape.disabled = false
	
	# Don't Want to Activate Whistle IF a Menu is UP!!!!
	if GameManager.MenuIsActive:
		return
	
	# Check If Whistle Button Is Pressed
	if Input.is_action_pressed('whistle'):
		if whistleRadius <= MaxWhistleRadius:
			# Increase Whistle Width
			SetWhistleCollisionRadiusTo(whistleRadius + WhistleSpeed)
			SetMeshScaleTo(whistleMeshScale.z + WhistleSpeed)

# Sets The Whistles X And Z Scale
func SetMeshScaleTo(value: float) -> void:
	WhistleMesh.scale.x = value
	WhistleMesh.scale.z = value
	WhistleMesh.scale.y = value

# Sets The Collision Shapes Radius
func SetWhistleCollisionRadiusTo(value: float) -> void:
	CollisionShape.set('radius', value)
	
# Sets Whistle Monitorable And Visible Properties
func SetWhistleMonitoringAndVisiblilityTo(active: bool) -> void:
	WhistleArea.monitoring = active
	$WhistleArea/WhistleShape.disabled = active
	WhistleArea.visible = active

func OnBodyEntered(body: Node3D) -> void:
	if body.StateMachine.CurrentState.name != "HelperStateGoHome":
		# Make The Helper Follow The Player By Changing States
		if body.global_transform.origin.y > body.FollowPoint.global_transform.origin.y:
			body.StateMachine.TransitionTo("HelperStatePhysicsFollow")
		else:
			body.StateMachine.TransitionTo("HelperStateFollow")
