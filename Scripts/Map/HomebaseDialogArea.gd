class_name HomebaseDialogArea
extends Node3D

func _ready() -> void:
	$Label3D.visible = false
	set_process_unhandled_input(false)

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("action"):
		EventBus.OnShowReturnToHomebaseMenu.emit()

func OnBodyEntered(body: Node3D) -> void:
	if body is Player:
		$Label3D.visible = true
		set_process_unhandled_input(true)

func OnBodyExited(body: Node3D) -> void:
	if body is Player:
		$Label3D.visible = false
		set_process_unhandled_input(false)
