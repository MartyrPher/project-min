class_name HelperButton
extends StaticBody3D

@export var HelpersRequired: int = 0 

var HelpersCurrent: int = 0

var IsPressed: bool = false

@onready var CountLabel: Label3D = $Label3d

signal Pressed()

func _ready() -> void:
	UpdateLabel()

func UpdateLabel() -> void:
	# Set The Label Text
	CountLabel.text = str(HelpersCurrent) + " / " + str(HelpersRequired)

func CheckAmountReached():
	UpdateLabel()
	if HelpersCurrent >= HelpersRequired:
		IsPressed = true
		Pressed.emit()
	elif IsPressed == true:
		IsPressed = false

func OnBodyEntered(_body: Node3D) -> void:
	HelpersCurrent += 1
	CheckAmountReached()

func OnBodyExited(_body: Node3D) -> void:
	HelpersCurrent -= 1
	CheckAmountReached()
