class_name HelperButtonObject
extends Node3D

@export var Buttons: Array

signal AllButtonsPressed()

func InitButtons():
	for buttonPath in Buttons:
		var button = get_node(buttonPath)
		button.Pressed.connect(OnButtonPressed.bind())

func OnButtonPressed():
	for buttonPath in Buttons:
		var button = get_node(buttonPath)
		if !button.IsPressed:
			return
	
	AllButtonsPressed.emit()
