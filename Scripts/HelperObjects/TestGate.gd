class_name Gate
extends HelperButtonObject

@onready var AnimPlayer: AnimationPlayer = $AnimationPlayer

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	InitButtons()
	AllButtonsPressed.connect(OnButtonsPressed.bind())
	
func OnButtonsPressed():
	AnimPlayer.play("DOWN")
