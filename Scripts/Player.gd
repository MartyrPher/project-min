class_name Player
extends CharacterBody3D

# Array Used To Keep Track Of Which
# Helpers Are In The Holding Range
var GrabAreaHelpers = []

# Spot To Move The HELD Helper To
@onready var HelperHoldMarker: Marker3D = $HelperHoldMarker

# Spot To Move The Follow Helper To
@onready var HelperFollowMarker: Marker3D = $HelperFollowMarker

@onready var HelperBlueSortMarker: Marker3D = $HelperBlueSortMarker
@onready var HelperYellowSortMarker: Marker3D = $HelperYellowSortMarker

@onready var NavigationAgent: NavigationAgent3D = $NavigationAgent3D

@export var ThrowCursor: Node3D

var HelperFollowPoints: Array

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

func _ready() -> void:
	CreateFollowMarkers()

func _unhandled_input(event):
	if event.is_action_released("throw"):
		if ThrowCursor.Disabled:
			return
			
		# Make Sure There IS A Helper To Throw
		if GrabAreaHelpers.size() > 0:
			# Grab The First Helper In The Array
			var helper = GrabAreaHelpers.pop_front()
			# Hold HIM, CHERISH HIM
			helper.StateMachine.TransitionTo("HelperStateCommand")

func CreateFollowMarkers():
	var points = GenerateSunflower(0)
	for point in points:
		var marker = Marker3D.new()
		$HelperFollowMarker.add_child(marker)
		marker.global_transform.origin = point
		
	for markers in $HelperFollowMarker.get_children():
		HelperFollowPoints.append(markers)

func GenerateSunflower(alpha: int = 0):
	var center = $HelperFollowMarker.global_transform.origin
	var maxPoints = GameManager.MaxHelper
	var goldenRatio = (1 + sqrt(5)) / 2
	var angleStride =  360 * goldenRatio
	
	var boundryPoints = alpha * sqrt(maxPoints)
	var points: Array = []
	for point in maxPoints:
		var radius = SunflowerRadius(point, maxPoints, boundryPoints)
		var theta = point * angleStride
		var x = 0.0 if is_nan(cos(theta) * radius) else cos(theta) * radius
		var z = 0.0 if is_nan(sin(theta) * radius) else sin(theta) * radius
		points.append(Vector3(center.x + x, center.y, center.z + z))
	return points

func SunflowerRadius(point, maxPoint, boundryPoint) -> float:
	return 1.0 if point > maxPoint - boundryPoint else sqrt(point - 0.5) / sqrt(maxPoint - (boundryPoint + 1) / 2)

func OnBodyEnteredGrabArea (body: Node3D) -> void:
	# Add The Helper To The Array Of Grabable (Is this a real word?) Helpers
	GrabAreaHelpers.append(body)

func OnBodyExitedGrabArea(body: Node3D) -> void:
	var helperIndex = GrabAreaHelpers.find(body)

	# Make Sure The Helper Is In Array
	if helperIndex != -1:
		GrabAreaHelpers.remove_at(helperIndex)
