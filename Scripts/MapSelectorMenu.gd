extends Control

@export var NodePathButtonDemoArea: NodePath

@export var NodePathButtonLevelOne: NodePath

var ButtonDemoArea: Button
var ButtonLevelOne: Button

# Called when the node enters the scene tree for the first time.
func _ready():
	ButtonDemoArea = get_node(NodePathButtonDemoArea)
	ButtonLevelOne = get_node(NodePathButtonLevelOne)

func OnButtonDemoAreaPressed():
	EventBus.OnDemoAreaButtonPressed.emit()
	EventBus.ShowMapSelectorMenu.emit()

func OnButtonLevelOnePressed():
	EventBus.OnLevelOnePressed.emit()
	EventBus.ShowMapSelectorMenu.emit()
