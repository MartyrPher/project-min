extends Node

signal OnSynthStationInteracted()

signal OnDemoAreaButtonPressed()
signal OnHomeBaseButtonPressed()
signal OnLevelOnePressed()

signal OnPausedPressed()

signal OnHelperCountChanged(difference: Dictionary)
signal OnHelperEnteredFollow()
signal OnHelperExitedFollow()

signal ShowHelperGetMenu(helpersInArea: Dictionary)
signal ShowMapSelectorMenu()
signal OnShowReturnToHomebaseMenu()

signal OnObjectGathered(resourceType: String, resourceAmount: int)

signal OnStaminaReachedHalf()
signal OnStaminaReachedQuarter()
signal OnStaminaDepleted()

signal OnGoneToSleep()

signal OnMapLoaded(hudEnabled: bool)
