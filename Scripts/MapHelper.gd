extends Node

@export var DemoAreaScene: PackedScene

@export var HomeBaseScene: PackedScene

@export var LevelOneScene: PackedScene

# Called when the node enters the scene tree for the first time.
func _ready():
	EventBus.OnDemoAreaButtonPressed.connect(ChangeMap.bind(DemoAreaScene))
	EventBus.OnHomeBaseButtonPressed.connect(ChangeMap.bind(HomeBaseScene))
	EventBus.OnLevelOnePressed.connect(ChangeMap.bind(LevelOneScene))
	
	EventBus.OnStaminaDepleted.connect(ChangeMap.bind(HomeBaseScene)) 

func ChangeMap(map: PackedScene) -> void:
	var thread = Thread.new()
	thread.start(RemoveChildMap.bind())

	thread.wait_to_finish()
	call_deferred("AddMap", map)
	
func AddMap(map: PackedScene):
	add_child(map.instantiate())

func RemoveChildMap():
	if (self.get_child_count() > 0):
		self.get_child(0).queue_free()
