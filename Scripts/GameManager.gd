extends Node

var MaxHelper: int = 48

var MenuIsActive = false

var HelpersInField: Dictionary = {
	"Red": 0,
	"Yellow": 0,
	"Blue": 0
}

var Inventory: Dictionary = {
	"Bronze": 0,
	"Silver": 0,
	"Gold": 0
}

var MaxStamina = 30
var CurrentDay = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	EventBus.OnHelperCountChanged.connect(OnHelperCountChanged.bind())
	EventBus.OnObjectGathered.connect(IncreaseInventory.bind())
	EventBus.OnGoneToSleep.connect(IncrementDay.bind())
	
	EventBus.OnHomeBaseButtonPressed.connect(ResetFieldCount.bind())
	EventBus.OnStaminaDepleted.connect(ResetFieldCount.bind()) 

func OnHelperCountChanged(difference: Dictionary):
	HelpersInField["Red"] += difference["Red"]
	HelpersInField["Yellow"] += difference["Yellow"]
	HelpersInField["Blue"] += difference["Blue"]
	
func IncreaseInventory(resourceType: String, resourceAmount: int):
	Inventory[resourceType] += resourceAmount
	
func IncrementDay():
	CurrentDay += 1
	
func ResetFieldCount() -> void:
	HelpersInField["Red"] = 0
	HelpersInField["Yellow"] = 0
	HelpersInField["Blue"] = 0

func GetCurrentDayString() -> String:
	return "Day " + str(CurrentDay)
	
func ChangeMouseMode(mode: Input.MouseMode):
	Input.mouse_mode = mode
	
func ChangeMenuIsActive(active: bool):
	MenuIsActive = active
