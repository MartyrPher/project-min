class_name HelperDestroyableObject
extends StaticBody3D

# The Helpers Required To Take Damage
@export var HelpersRequired: int = 0

# Amount Of Health For The Object
@export var Health: float = 0

# Takes Damage At The Interval
@export var DamageTime: float = 5
# Amount Of Damage That The Object Takes
@export var DamageTaken: int = 0

# Resource Type Added To Inventory When Destroyed
@export var ResourceType: String = "Bronze"

# Resource Amount Added To Inventory When Destroyed
@export var ResourceAmount: int = 0

# Current Helper Count In Object
var CurrentHelperCount: int = 0
var CurrentHelperArray: Array

# Label For Current / Required Helpers
@onready var CountLabel: Label3D = $Label3d

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	# Update The Label
	UpdateLabel()
	
	# Set The Health Max Value And Value
	$HealthBar.InitializeHealthBar(Health)
	$HealthBar.TweenFinished.connect(DestroyObject.bind())
	
	# Connect Damage Timer To Damage Function
	$DamageTimer.connect("timeout", Damage.bind())
	

func OnHelperEntered(body: Node3D) -> void:
	if body is Helper:
		# IF The Helper Is Not Following The Player
		if body.StateMachine.CurrentState.name == "HelperStateFollow":
			return
			
		# Add Helper To Array
		CurrentHelperArray.append(body)
		body.StateMachine.TransitionTo("HelperStateIdle")

		# Increment The Helper Count
		CurrentHelperCount += 1
		# Update Label
		UpdateLabel()
		
		if CurrentHelperCount >= HelpersRequired:
			# Start Taking Damage IF Helper Requirement Reached
			StartDamage()
		
func OnHelperExited(body: Node3D) -> void:
	if body is Helper:
		# Find The Helper In The Array.
		# If not in the Array means that they are not doing damage
		var helperIndex = CurrentHelperArray.find(body)
		if helperIndex == -1:
			return
		
		# Remove Helper When Exiting Area
		CurrentHelperArray.remove_at(helperIndex)
		# Subtract Count When Helper Exits Object
		CurrentHelperCount -= 1
		
		# Ensure Count Is NOT Negative
		if CurrentHelperCount < 0:
			CurrentHelperCount = 0
			
		# Update Label
		UpdateLabel()
		
		# IF the Helper Requirement Is Not Met, Stop Damage
		if CurrentHelperCount < HelpersRequired:
				StopDamage()

func UpdateLabel() -> void:
	# Set The Label Text
	CountLabel.text = str(CurrentHelperCount) + " / " + str(HelpersRequired)
	
func Damage() -> void:
	print("Took Damage")
	# Subtract Health By Damage
	Health -= DamageTaken
	$HealthBar.SetHealthValue(Health)
		
func DestroyObject():
	# Destroy The Object When Health Reaches 0
	if Health <= 0:
		ObjectBroken()
	
func StartDamage() -> void:
	# Start The Damage Timer
	$DamageTimer.start(DamageTime)
	
func StopDamage() -> void:
	# Stop The Damage Timer
	$DamageTimer.stop()
	
func ObjectBroken() -> void:
	print("I'm broken")
	EventBus.OnObjectGathered.emit(ResourceType, ResourceAmount)
	# Kill
	queue_free()
