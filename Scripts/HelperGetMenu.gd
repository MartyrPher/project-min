class_name HelperGetMenu
extends Control

@export var NodePathLabelStored: NodePath
@export var NodePathLabelCurrentRed: NodePath
@export var NodePathLabelCurrentYellow: NodePath
@export var NodePathLabelCurrentBlue: NodePath
@export var NodePathLabelFieldRed: NodePath
@export var NodePathLabelFieldYellow: NodePath
@export var NodePathLabelFieldBlue: NodePath

@export var NodePathStoreButtonRed: NodePath
@export var NodePathTakeButtonRed: NodePath
@export var NodePathStoreButtonYellow: NodePath
@export var NodePathTakeButtonYellow: NodePath
@export var NodePathStoreButtonBlue: NodePath
@export var NodePathTakeButtonBlue: NodePath

var LabelStored: Label
var LabelCurrentRed: Label
var LabelCurrentYellow: Label
var LabelCurrentBlue: Label
var LabelFieldRed: Label
var LabelFieldYellow: Label
var LabelFieldBlue: Label

var ButtonStoreRed: TextureButton
var ButtonTakeRed: TextureButton
var ButtonStoreYellow: TextureButton
var ButtonTakeYellow: TextureButton
var ButtonStoreBlue: TextureButton
var ButtonTakeBlue: TextureButton

@onready var CountStored: int = GameManager.MaxHelper
var CountCurrentRed: int = 0
var CountCurrentYellow: int = 0
var CountCurrentBlue: int = 0

# Used For Calculating The Difference
var InitialCurrentRed: int = 0
var InitialCurrentYellow: int = 0
var InitialCurrentBlue: int = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	ButtonStoreRed = get_node(NodePathStoreButtonRed)
	ButtonTakeRed = get_node(NodePathTakeButtonRed)
	ButtonStoreYellow = get_node(NodePathStoreButtonYellow)
	ButtonTakeYellow = get_node(NodePathTakeButtonYellow)
	ButtonStoreBlue = get_node(NodePathStoreButtonBlue)
	ButtonTakeBlue = get_node(NodePathTakeButtonBlue)
	
	LabelStored = get_node(NodePathLabelStored)
	LabelCurrentRed = get_node(NodePathLabelCurrentRed)
	LabelCurrentYellow = get_node(NodePathLabelCurrentYellow)
	LabelCurrentBlue = get_node(NodePathLabelCurrentBlue)
	LabelFieldRed = get_node(NodePathLabelFieldRed)
	LabelFieldYellow = get_node(NodePathLabelFieldYellow)
	LabelFieldBlue = get_node(NodePathLabelFieldBlue)
	
	LabelStored.text = str(CountStored)
	
	SetStoreButtons()
	SetTakeButtons()
	
	EventBus.OnHomeBaseButtonPressed.connect(ResetMenu.bind())
	EventBus.OnStaminaDepleted.connect(ResetMenu.bind()) 

func UpdateCount(newCount):
	print(newCount)
	InitialCurrentRed = newCount["Red"]
	InitialCurrentYellow = newCount["Yellow"]
	InitialCurrentBlue = newCount["Blue"]
	
	CountCurrentRed = newCount["Red"]
	CountCurrentYellow = newCount["Yellow"]
	CountCurrentBlue = newCount["Blue"]
	
	LabelCurrentRed.text = str(CountCurrentRed)
	LabelCurrentYellow.text = str(CountCurrentYellow)
	LabelCurrentBlue.text = str(CountCurrentBlue)
	
	LabelFieldRed.text = str(GameManager.HelpersInField["Red"])
	LabelFieldYellow.text = str(GameManager.HelpersInField["Yellow"])
	LabelFieldBlue.text = str(GameManager.HelpersInField["Blue"])
	
	SetStoreButtons()
	SetTakeButtons()

func OnButtonOkayPressed():
	var differenceRed = CountCurrentRed - InitialCurrentRed
	var differenceYellow = CountCurrentYellow - InitialCurrentYellow
	var differenceBlue =  CountCurrentBlue - InitialCurrentBlue
	var difference: Dictionary = {
		"Red": differenceRed,
		"Yellow": differenceYellow,
		"Blue": differenceBlue
	}
	
	var totalDifference = 0
	for color in difference.size():
		totalDifference += color
	
	# Don't Want To Notify If There Was No Change
	if totalDifference == 0:
		return
		
	EventBus.OnHelperCountChanged.emit(difference)
	
	visible = false

func ResetMenu() -> void:
	CountCurrentRed = 0
	CountCurrentYellow = 0
	CountCurrentBlue = 0

	# Used For Calculating The Difference
	InitialCurrentRed = 0
	InitialCurrentYellow = 0
	InitialCurrentBlue= 0
	
	LabelFieldRed.text = "0"
	LabelFieldYellow.text = "0"
	LabelFieldBlue.text = "0"
	
	LabelStored.text = str(GameManager.MaxHelper)

func OnButtonNoPressed():
	visible = false

func OnButtonStoreRed():
	CountCurrentRed -= 1
	HandleStoreButton(CountCurrentRed, LabelCurrentRed, ButtonStoreRed)

func OnButtonTakeRed():
	CountCurrentRed += 1
	HandleTakeButton(CountCurrentRed, LabelCurrentRed, ButtonStoreRed)

func OnButtonStoreYellow():
	CountCurrentYellow -= 1
	HandleStoreButton(CountCurrentYellow, LabelCurrentYellow, ButtonStoreYellow)
	
func OnButtonTakeYellow():
	CountCurrentYellow += 1
	HandleTakeButton(CountCurrentYellow, LabelCurrentYellow, ButtonStoreYellow)

func OnButtonStoreBlue():
	CountCurrentBlue -= 1
	HandleStoreButton(CountCurrentBlue, LabelCurrentBlue, ButtonStoreBlue)
	
func OnButtonTakeBlue():
	CountCurrentBlue += 1
	HandleTakeButton(CountCurrentBlue, LabelCurrentBlue, ButtonStoreBlue)
	
func HandleStoreButton(CountColor: int, LabelColor: Label, ButtonColor: TextureButton):
	CountStored += 1
	LabelColor.text = str(CountColor)
	LabelStored.text = str(CountStored)
	
	if CountColor == 0:
		ButtonColor.disabled = true
		
	if CountStored > 0:
		ButtonTakeRed.disabled = false
		ButtonTakeYellow.disabled = false
		ButtonTakeBlue.disabled = false

func HandleTakeButton(CountColor:int, LabelColor: Label, ButtonColor: TextureButton):
	CountStored -= 1
	LabelColor.text = str(CountColor)
	LabelStored.text = str(CountStored)
	
	if CountStored == 0:
		ButtonTakeRed.disabled = true
		ButtonTakeYellow.disabled = true
		ButtonTakeBlue.disabled = true
		
	if CountColor > 0:
		ButtonColor.disabled = false

func SetStoreButtons():
	if CountCurrentRed == 0:
		ButtonStoreRed.disabled = true
	else:
		ButtonStoreRed.disabled = false
		
	if CountCurrentYellow == 0:
		ButtonStoreYellow.disabled = true
	else:
		ButtonStoreYellow.disabled = false
		
	if CountCurrentBlue == 0:
		ButtonStoreBlue.disabled = true
	else:
		ButtonStoreBlue.disabled = false

func SetTakeButtons():
	if CountStored == 0:
		ButtonTakeRed.disabled = true
		ButtonTakeYellow.disabled = true
		ButtonTakeBlue.disabled = true
	else:
		ButtonTakeRed.disabled = false
		ButtonTakeYellow.disabled = false
		ButtonTakeBlue.disabled = false
