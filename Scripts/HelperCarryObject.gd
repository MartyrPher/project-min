class_name HelperCarryObject
extends Path3D

@export var HelpersRequired: int = 0
@export var SPEED: float = 2

# Resource Type Added To Inventory When Destroyed
@export var ResourceType: String = "Bronze"
# Resource Amount Added To Inventory When Destroyed
@export var ResourceAmount: int = 0

var CurrentHelperCount: int = 0

var HelperCarryPoints: Array

@onready var CountLabel: Label3D = $PathFollow3D/HelperCarryObject/Label3d
@onready var StaticCarryObject: StaticBody3D = $PathFollow3D/HelperCarryObject
@onready var MeshInstance: MeshInstance3D = $PathFollow3D/HelperCarryObject/MeshInstance3d
@onready var CenterMarker: Marker3D = $PathFollow3D/HelperCarryObject/CenterMarker

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	UpdateLabel()
	set_physics_process(false)
	CreateFollowMarkers()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(_delta) -> void:
	$PathFollow3D.progress += SPEED * _delta
	
	if $PathFollow3D.progress_ratio == 1:
		EventBus.OnObjectGathered.emit(ResourceType, ResourceAmount)
		queue_free()
	
func UpdateLabel() -> void:
	CountLabel.text = str(CurrentHelperCount) + " / " + str(HelpersRequired)

func AssignPoint() -> Marker3D:
	var point = HelperCarryPoints.pop_front()
	HelperCarryPoints.push_back(point)
	return point

func CreateFollowMarkers():
	var points = GenerateCarryPoints()
	for point in points:
		var marker = Marker3D.new()
		CenterMarker.add_child(marker)
		marker.global_transform.origin = point
		
	for markers in CenterMarker.get_children():
		HelperCarryPoints.append(markers)

func GenerateCarryPoints() -> Array:
	var center = StaticCarryObject.global_transform.origin
	var points: Array = []
	for helperPoint in HelpersRequired:
		var theta = (PI * 2) / HelpersRequired
		var angle = theta * helperPoint
		var point: Vector3 = Vector3.ZERO
		point.x = center.x + (0.6 * cos(angle))
		point.z = center.z + (0.6 * sin(angle))
		point.y = 0.25
		points.append(point)
	return points

func UpdateCarryCount(value: int) -> void:
	CurrentHelperCount += value
	UpdateLabel()
	
	if CurrentHelperCount >= HelpersRequired:
		MeshInstance.transform.origin.y = 0.25
		set_physics_process(true)
		
	if CurrentHelperCount < HelpersRequired:
		MeshInstance.transform.origin.y = 0
		set_physics_process(false)

func OnBodyEntered(body):
	if body is Helper:
		if body.StateMachine.CurrentState.name == "HelperStateFollow":
			return
		
		if body.StateMachine.CurrentState.name == "HelperStatePickUp":
			return
		
		body.StateMachine.TransitionTo("HelperStatePickUp", { "Object": self })
