class_name Bed
extends Node3D

func _ready():
	set_process_unhandled_input(false)

func _unhandled_input(event):
	if event.is_action_pressed("action"):
		EventBus.OnGoneToSleep.emit()

func OnBodyEntered(body):
	if body is Player:
		set_process_unhandled_input(true)

func OnBodyExited(body):
	if body is Player:
		set_process_unhandled_input(false)
