class_name RunAwayArea
extends Area3D

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	body_entered.connect(OnBodyEntered.bind())

func OnBodyEntered(body: Node3D) -> void:
	body.StateMachine.TransitionTo("HelperStateRunAway")
