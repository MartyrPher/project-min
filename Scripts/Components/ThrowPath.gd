class_name ThrowPath
extends Path3D

var THROWSPEED = 15

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$PathFollow3D.progress += THROWSPEED * delta
	
func GetPathTransform() -> Transform3D:
	return $PathFollow3D.global_transform

func CreateCurve(cursorPoint: Vector3):
	var distance = transform.origin.distance_to(cursorPoint)
	print("Path Origin", transform.origin)
	print("CursorPoint", cursorPoint)
	
	var calculatedCurve = Curve3D.new()
	var middlePoint = Vector3(0, 1.3, (-distance / 2.1))
	var middlePointInOut = Vector3(0, 0.26, 1.1)
	
	var endPoint2 = Vector3(0, -1, -distance)
	
	calculatedCurve.add_point(Vector3.ZERO)
	calculatedCurve.add_point(middlePoint, middlePointInOut, -middlePointInOut)
	calculatedCurve.add_point(endPoint2)
	self.curve = calculatedCurve
