class_name Cursor
extends CharacterBody3D

# Camera Instance
var Camera: GameCamera

# Player Instance
var PlayerInstance: Player

# Disabled: See MaxCommandDistance For Disabled Range
var Disabled: bool = false

# Max Distance The Cursor Can Go Before Being Disabled
var MaxCommandDistance: int = 8

func _ready() -> void:
	Camera = get_tree().get_first_node_in_group("Camera") as GameCamera
	PlayerInstance = get_tree().get_first_node_in_group("Player") as Player

func GetRayCastPoint() -> Vector3:
	return $RayCast3D.get_collision_point()
	
func ModulateRed() -> void:
	$Decal.modulate = Color.RED

func ModulateWhite() -> void:
	$Decal.modulate = Color.WHITE
	
func ModulateLineRed() -> void:
	$DecalCommandLine.modulate = Color.RED

func ModulateLineWhite() -> void:
	$DecalCommandLine.modulate = Color.WHITE
	
func CalcualteCommandLine(length: float):
	$DecalCommandLine.transform.origin.z = -(length / 2)
	$DecalCommandLine.extents.z = length / 2

func EnableCommandDecal():
	$DecalCommandLine.visible = true

func DisableCommandDecal():
	$DecalCommandLine.visible = false

func _process(_delta: float) -> void:
	if Camera == null:
		Camera = get_tree().get_first_node_in_group("Camera") as GameCamera
		
	if PlayerInstance == null:
		PlayerInstance = get_tree().get_first_node_in_group("Player") as Player

	var mousePosition = get_viewport().get_mouse_position()
	var worldPosition = Camera.GetMousePosition3DCoords(mousePosition)
	var distance = global_transform.origin.distance_to(PlayerInstance.HelperHoldMarker.global_transform.origin)
	
	if distance > MaxCommandDistance:
		Disabled = true
		ModulateRed()
		ModulateLineRed()
	elif Disabled:
		Disabled = false
		ModulateWhite()
		ModulateLineWhite()
		
	transform.origin = worldPosition
	
	if $DecalCommandLine.visible and distance > 2:
		look_at(PlayerInstance.global_transform.origin)
		CalcualteCommandLine(distance)

	if Input.is_action_pressed("throw") and distance > 2:
		EnableCommandDecal()
	elif Input.is_action_just_released("throw") or distance < 2:
		DisableCommandDecal()
