class_name HealthBar
extends Node3D

var health = 0

var maxHealth = 0

signal TweenFinished()

func _ready():
	%ProgressBar.max_value = maxHealth
	%ProgressBar.value = health

func InitializeHealthBar(value: float) -> void:
	%ProgressBar.max_value = value
	%ProgressBar.value = value

func SetHealthValue(value: float) -> void:
	var barTween = create_tween()
	barTween.tween_property(%ProgressBar, "value", value, 1)
	barTween.finished.connect(TweenHealthFinished.bind())

func TweenHealthFinished():
	TweenFinished.emit()
