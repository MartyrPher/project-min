class_name SynthStation
extends Node3D

var PlayerInArea: bool = false

func _unhandled_input(event):
	if event.is_action_pressed("action") and PlayerInArea:
		EventBus.OnSynthStationInteracted.emit()

func OnBodyEntered(body):
	if body is Player:
		$Label3d.visible = true
		PlayerInArea = true

func OnBodyExited(body):
	if body is Player:
		$Label3d.visible = false
		PlayerInArea = false
