class_name FullscreenMessage
extends Control

var MessageLabel: Label

var StartAlpha: int
var EndAlpha: int

func _ready():
	TweenMessageIn()

func Init():
	modulate.a8 = 0
	MessageLabel = $Label

func ChangeAlpha(startAlpha: int, endAlpha: int):
	StartAlpha = startAlpha
	EndAlpha = endAlpha

func UpdateLabel(newText: String):
	if MessageLabel == null:
		Init()
	
	MessageLabel.text = newText

func TweenMessageIn():
	var tween: Tween = create_tween()
	tween.finished.connect(StartExitTween.bind())
	tween.tween_property(self, "modulate:a8", StartAlpha, 3)

func TweenMessageOut():
	var tween: Tween = create_tween()
	tween.finished.connect(Exit.bind())
	tween.tween_property(self, "modulate:a8", EndAlpha, 3)
	
func StartExitTween():
	TweenMessageOut()
	
func Exit():
	queue_free()
