class_name StaminaBar
extends Control

@onready var StaminaBar: ProgressBar = $MarginContainer/ProgressBar
@onready var StaminaTimer: Timer = $StaminaTimer

var CountdownTime: int = 10
var CountdownProgress: int = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	ResetValue()
	StaminaTimer.timeout.connect(DecreaseValue.bind())
	
	EventBus.OnGoneToSleep.connect(RestartStamina.bind())
	
func StartTimer():
	StaminaTimer.start(CountdownTime)

func StopTimer():
	StaminaTimer.stop()

func RestartStamina():
	ResetValue()
	StopTimer()

func ResetValue():
	StaminaBar.max_value = GameManager.MaxStamina
	UpdateValue(StaminaBar.max_value)
	
func UpdateValue(newValue: int):
	StaminaBar.value = newValue
	
func DecreaseValue():
	var barTween: Tween = create_tween()
	var newValue = StaminaBar.value - CountdownProgress
	barTween.tween_property(StaminaBar, "value", newValue, 1).set_trans(Tween.EASE_IN)
	
	if StaminaBar.value != 0:
		StartTimer()

func OnProgressValueChanged(value):
	var middleValue = StaminaBar.max_value / 2
	var quarterValue = StaminaBar.max_value / 4
	
	if StaminaBar.value == middleValue:
		EventBus.OnStaminaReachedHalf.emit()
		
	if StaminaBar.value == quarterValue:
		EventBus.OnStaminaReachedQuarter.emit()

	if value <= 0:
		EventBus.OnStaminaDepleted.emit()
