class_name PauseMenu
extends Control

func _unhandled_input(event):
	if (event.is_action_pressed("pause")):
		EventBus.OnPausedPressed.emit()
