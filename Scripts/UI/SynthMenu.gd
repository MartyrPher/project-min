extends Control

@export var SynthResources: Array

@export var NodePathLabelTitle: NodePath
@export var NodePathLabelDescription: NodePath
@export var NodePathLabelBronze: NodePath
@export var NodePathLabelSilver: NodePath
@export var NodePathLabelGold: NodePath

@export var NodePathGridContainer: NodePath

var LabelTitle: Label
var LabelDescription: Label
var LabelBronze: Label
var LabelSilver: Label
var LabelGold: Label

var ButtonGrid: GridContainer

# Called when the node enters the scene tree for the first time.
func _ready():
	LabelTitle = get_node(NodePathLabelTitle)
	LabelDescription = get_node(NodePathLabelDescription)
	LabelBronze = get_node(NodePathLabelBronze)
	LabelSilver = get_node(NodePathLabelSilver)
	LabelGold = get_node(NodePathLabelGold)
	
	ButtonGrid = get_node(NodePathGridContainer)
	for button in ButtonGrid.get_children():
		button.pressed.connect(UpdateSidebar.bind(button))

func UpdateSidebar(button: Button):
	var index = ButtonGrid.get_children().find(button)
	var synthResource = SynthResources[index]
	
	LabelTitle.text = synthResource.Title
	LabelDescription.text = synthResource.Description
	LabelBronze.text = "x" + str(synthResource.RequiredBronze)
	LabelSilver.text = "x" + str(synthResource.RequiredSilver)
	LabelGold.text = "x" + str(synthResource.RequiredGold)
