class_name ItemAddedNotif
extends Panel

func _ready():
	get_tree().create_timer(3).timeout.connect(OnTimeout.bind())

func InitNotif(type: String, amount: int) -> void:
	$Label.text = "x" + str(amount) + " " + type

func OnTimeout():
	queue_free()
