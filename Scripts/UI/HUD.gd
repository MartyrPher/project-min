class_name HUD
extends Control

@onready var LabelTotalField: Label = $MarginContainer/HBoxContainer/LabelTotalField
@onready var LabelFollowing: Label = $MarginContainer/HBoxContainer/LabelFollowing

var CurrentFollowing: int = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	EventBus.OnHelperCountChanged.connect(UpdateFieldCount.bind())
	EventBus.OnHelperEnteredFollow.connect(IncreaseFollowCount.bind())
	EventBus.OnHelperExitedFollow.connect(DecreaseFollowCount.bind())
	
	EventBus.OnHomeBaseButtonPressed.connect(ResetHelperCounts.bind())
	EventBus.OnStaminaDepleted.connect(ResetHelperCounts.bind()) 

func EnableHUD():
	visible = true
	$StaminaBar.StartTimer()
	
func DisableHUD():
	visible = false
	$StaminaBar.StopTimer()

func UpdateFieldCount(_difference: Dictionary):
	var total: int = 0
	for helper in GameManager.HelpersInField.values():
		total += helper

	if total < 0:
		total = 0
	
	LabelTotalField.text = str(total)
	
func IncreaseFollowCount():
	CurrentFollowing += 1
	UpdateFollowLabel(CurrentFollowing)

func DecreaseFollowCount():
	CurrentFollowing -= 1
	UpdateFollowLabel(CurrentFollowing)

func ResetHelperCounts():
	CurrentFollowing = 0
	LabelFollowing.text = "0"
	LabelTotalField.text = "0"

func UpdateFollowLabel(newCount: int):
	LabelFollowing.text = str(newCount)
