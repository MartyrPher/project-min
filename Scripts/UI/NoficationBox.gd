class_name NotficationBox
extends Control

@export var PackedSceneAddItemNotif: PackedScene
@export var NodePathNotificationContainer: NodePath

var NotificationContainer: VBoxContainer

# Called when the node enters the scene tree for the first time.
func _ready():
	NotificationContainer = get_node(NodePathNotificationContainer)
	
	EventBus.OnObjectGathered.connect(AddItemNotification.bind())

func AddItemNotification(resourceType: String, resourceAmount: int):
	var addNotification = PackedSceneAddItemNotif.instantiate() as ItemAddedNotif
	addNotification.InitNotif(resourceType, resourceAmount)
	NotificationContainer.add_child(addNotification)
