extends Resource

@export var Title: String
@export var Description: String
@export var RequiredBronze: int
@export var RequiredSilver: int
@export var RequiredGold: int
