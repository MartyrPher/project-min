class_name ConfirmHomebaseReturnMenu
extends Control

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.

func OnYesPressed() -> void:
	EventBus.OnHomeBaseButtonPressed.emit()
	EventBus.OnShowReturnToHomebaseMenu.emit()

func OnNoPressed() -> void:
	EventBus.OnShowReturnToHomebaseMenu.emit()
