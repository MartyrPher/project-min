class_name Map
extends Node3D

func MapFinishedReady(hudEnabled: bool):
	EventBus.OnMapLoaded.emit(hudEnabled)
