extends Map

func _ready():
	MapFinishedReady(false)

func OnBodyEnteredMapArea(body):
	if body is Player:
		EventBus.ShowMapSelectorMenu.emit()
