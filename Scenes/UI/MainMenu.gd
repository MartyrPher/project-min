class_name MainMenu
extends Node3D

func OnDemoAreaButtonPressed():
	queue_free()
	EventBus.OnDemoAreaButtonPressed.emit()

func OnHomeBaseButtonPressed():
	queue_free()
	EventBus.OnHomeBaseButtonPressed.emit()

func OnLevelOnePressed():
	queue_free()
	EventBus.OnLevelOnePressed.emit()
